const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
const mongoose = require("mongoose");
const UserSchema = require("./esquema/User");
const key = require("./config/default.json");
const passport = require("passport");

const opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = key.jwtSecret;

module.exports = function(passport) {
  passport.use(
    new JwtStrategy(opts, (jwt_payload, done) => {
      if (!jwt_payload.GoogleId) {
        UserSchema.findById(jwt_payload.id)
          .then(user => {
            if (user) {
              return done(null, user);
            }
            return done(null, false);
          })
          .catch(err => console.log(err));
      } else {
        UserSchema.find({
          GoogleId: jwt_payload.GoogleId
        })
          .then(user => {
            if (user) {
              return done(null, user[0]);
            }
            return done(null, false);
          })
          .catch(err => console.log(err));
      }
    })
  );
};
