const express = require("express");
const router = express.Router();
const ItinerarySchema = require("../esquema/Itinerary");
router.get("/", async (req, res) => {
  const Itinerary = await ItinerarySchema.find();
  res.json(Itinerary);
});
router.get("/:cityid", async (req, res) => {
  const cityid = req.params.cityid;
  const Itinerary = await ItinerarySchema.find({ cityid });
  res.json(Itinerary);
});
module.exports = router;
