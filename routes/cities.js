const express = require("express");
const router = express.Router();
const CityEsquema = require("../esquema/City");
router.get("/", async (req, res) => {
  const City = await CityEsquema.find();
  res.json(City);
});
module.exports = router;
