const express = require("express");
const router = express.Router();
const ActividadEsquema = require("../esquema/activities");
router.get("/:idItinerary", async (req, res) => {
  const idItinerary = req.params.idItinerary;
  const Actividad = await ActividadEsquema.find({ idItinerary: idItinerary });

  res.json(Actividad);
});
router.post("/", async (req, res) => {
  const activities = req.body;
  const newactivity = await ActividadEsquema.create(activities);
  res.json(newactivity);
});
module.exports = router;
