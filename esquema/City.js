const mongoose = require("mongoose");
const { Schema } = mongoose;
const CityEsquema = new Schema({
  country: { type: String, required: true },
  city: { type: String, required: true },
  img: { type: String, required: true }
});
module.exports = mongoose.model("cities", CityEsquema);
