const express = require("express");
const app = express();
const morgan = require("morgan");
const path = require("path");
const { mongoose } = require("./database");
const cors = require("cors");
const port = process.env.PORT || 5000;
const passport = require("passport");

require("./passport")(passport);

app.use(morgan("dev"));
app.use(express.json());
app.use(cors());

app.use(passport.initialize());
app.use(passport.session());

app.use("/routejson/cities", require("./routes/cities"));
app.use("/routejson/itineraries", require("./routes/itineraries"));
app.use("/routejson/activities", require("./routes/activities"));
app.use("/routejson/users", require("./routes/users"));
app.use("/routejson/login", require("./routes/login"));
app.use("/routejson/auth/google", require("./auth/auth"));
app.use(express.static(path.join(__dirname, "cliente", "public")));
app.listen(port);
