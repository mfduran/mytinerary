import { combineReducers } from "redux";
import cityreducer from "./cityreducer";
import errorReducer from "./errorReducer";
import authReducer from "./authReducer";
import itineraryreducer from "./itineraryreducer";
export default combineReducers({
  cityreducer,
  itineraryreducer,
  authReducer,
  errorReducer
});
