import { ALLCITIES } from "../actions/types";

const initialState = {
  citys: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case ALLCITIES:
      return {
        ...state,
        citys: action.payload
      };
    default:
      return state;
  }
}
