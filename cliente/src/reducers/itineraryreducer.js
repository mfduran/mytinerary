import { ALLITINERARIES} from "../actions/types";

const initialState = {
  Itineraries: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case ALLITINERARIES:
      return {
        ...state,
        Itineraries: action.payload
      };
    default:
      return state;
  }
}
