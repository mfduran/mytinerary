import { ALLACTIVITIES, LOADINGAC, ERROR } from "../actions/types";

const initialState = {
  Activities: [],
  loading: false,
  error: ""
};

export default function(state = initialState, action) {
  switch (action.type) {
    case ALLACTIVITIES:
      return {
        ...state,
        Activities: action.payload
      };
    default:
      return state;
  }
}
