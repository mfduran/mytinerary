import { ALLCITIES } from "./types";
import axios from "axios";

export const getCitys = () => dispach => {
  axios
    .get("http://localhost:5000/routejson/cities")
    .then(data => {
      dispach({
        type: ALLCITIES,
        payload: data.data
      });
    })
    .catch(err => {
      console.log("Error; err");
    });
};
