import { ALLACTIVITIES, LOADINGAC, ERROR } from "./types";
import axios from "axios";

export const getActivities = idItinerary => dispach => {
  dispach({
    type: LOADINGAC
  });
  axios
    .get(`http://localhost:5000/routejson/activities/${idItinerary}`)
    .then(data => {
      dispach({
        type: ALLACTIVITIES,
        payload: data.data
      });
    });
};
