import axios from "axios";
import { ALLITINERARIES, LOADINGIT } from "./types";

export const getItineraries = cityid => dispach => {
  dispach({
    type: LOADINGIT
  });
  axios
    .get(`http://localhost:5000/routejson/itineraries/${cityid}`)
    .then(data => {
      dispach({
        type: ALLITINERARIES,
        payload: data.data
      });
    });
};
