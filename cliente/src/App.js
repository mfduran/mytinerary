import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import NavBar from "./components/NavBar";
import Body from "./components/Body";
import Cities from "./components/CitiesPage";
import Login from "./components/Login";
import CreateAccount from "./components/CreateAccount";
import store from "./store";
import { Provider } from "react-redux";
import Itineraries from "./components/ItinerariesPage";
import { loadUser } from "./actions/authActions";
import VerifyToken from "./components/verifytoken";

class App extends Component {
  componentDidMount() {
    store.dispatch(loadUser());
  }
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <div className="container-fluid pl-0 pr-0">
            <NavBar />
            <Switch>
              <Route exact path="/verifytoken/:token" component={VerifyToken} />
              <Route exact path="/" component={Body} />
              <Route exact path="/citiespage" component={Cities} />
              <Route exact path="/itinerary/:cityid" component={Itineraries} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/createAcc" component={CreateAccount} />
            </Switch>
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}
export default App;
