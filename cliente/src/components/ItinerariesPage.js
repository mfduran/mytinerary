import React, { Component } from "react";
import { connect } from "react-redux";
import { getItineraries } from "../actions/itinerariesActions";
import Banner from "./banner";
import { getCitys } from "../actions/citiesActions";
import Footer from "./Footer";
import Activities from "./activities";

class Itineraries extends Component {
  renderItineraries() {
    return this.props.itineraryreducer.Itineraries.length !== 0 ? (
      this.props.itineraryreducer.Itineraries.map(itinerary => (
        <div>
          <div
            className="container-fluid media border-right border-top border-left d-flex mt-5"
            key={itinerary.itinerary}
          >
            <img
              src={itinerary.profilePic}
              alt="Guia Turistica"
              className="align-self-start mr-3 rounded-circle"
              style={{ width: "100px" }}
            ></img>

            <div className="media-body ">
              <h3 className="mt-0">{itinerary.title}</h3>
              <div className="d-flex justify-content-between">
                {" "}
                <span>Likes: {itinerary.rating}</span>
                <span>Precio: {itinerary.price}$</span>
                <span>Duración: {itinerary.duration}</span>
              </div>
              {itinerary.hashtag.map((tag, index) => (
                <span> {tag}</span>
              ))}
            </div>
          </div>
          <button
            type="button"
            className="h5 w-100"
            data-toggle="collapse"
            data-target="#demo"
          >
            View All
          </button>
          <div id="demo" class="collapse"></div>
          <Activities
            idItinerary={itinerary._id}
            key={`slider${itinerary._id}`}
          />
        </div>
      ))
    ) : (
      <h3>No Hay Itinerarios Disponibles</h3>
    );
  }
  componentDidMount() {
    const cityid = this.props.match.params.cityid;
    this.props.getItineraries(cityid);
    this.props.getCitys();
  }
  render() {
    var NewArray = [];
    NewArray = this.props.cityreducer.citys.filter(array => {
      if (array._id === this.props.match.params.cityid) {
        return array;
      }
      return false;
    });
    return (
      <div>
        <Banner city={NewArray[0]} />
        <div className="container mb-5 mt-2">
          {" "}
          {this.props.itineraryreducer.loading ? (
            <h3>Loading</h3>
          ) : this.props.itineraryreducer.error ? (
            <h3>{this.props.itineraryreducer.error}</h3>
          ) : this.props.itineraryreducer.Itineraries ? (
            this.renderItineraries()
          ) : (
            ""
          )}
        </div>
        <Footer />;
      </div>
    );
  }
}
const mapDispatchToProps = {
  getItineraries,
  getCitys
};
const mapStateToProps = ({ itineraryreducer, cityreducer }) => {
  return { itineraryreducer, cityreducer };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Itineraries);
