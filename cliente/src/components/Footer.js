import React, { Component } from "react";
import { FaHome } from "react-icons/fa";
import { IoIosArrowBack } from "react-icons/io";
import { Link } from "react-router-dom";

class Footer extends Component {
  render() {
    return (
      <h1 className="d-flex justify-content-center fixed-bottom bg-dark">
        <div className="ml-3 col-sm-6">
          {" "}
          <Link to="/CitiesPage">
            <IoIosArrowBack />
          </Link>{" "}
        </div>
        <div className="mr-5 col-sm-6">
          {" "}
          <Link to="/">
            {" "}
            <FaHome />
          </Link>
        </div>
      </h1>
    );
  }
}
export default Footer;
