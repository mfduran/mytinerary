import "slick-carousel/slick/slick.css";
import "./Carrusel.css";
import "slick-carousel/slick/slick-theme.css";
import React, { Component } from "react";
import Slider from "react-slick";
import Barcelona from "../images/sagrada-familia-barcelona-800x477.jpg";
import NewYork from "../images/Nueva-York-3.jpg";
import Maracaibo from "../images/Maracaibo-1.jpg";
import Valencia from "../images/valencia.jpg";
import Tokyo from "../images/tokyo.jpg";
import Grecia from "../images/grecia.jpg";
import Madrid from "../images/madrid.jpg";
import Dubai from "../images/dubai.jpg";
import Paris from "../images/paris.jpg";
import Venecia from "../images/venecia.jpg";
import Estambul from "../images/estambul.jpg";
import Londres from "../images/londres.jpg";
class Carrusel extends Component {
  render() {
    var settings = {
      arrows: true,
      dots: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1
    };

    return (
      <div id="galeria">
        <p>Popular MYtineraries</p>
        <Slider {...settings}>
          <div>
            <div class="columnaizq">
              <div class="ciudad">
                <img src={Barcelona} alt="Barcelona" />
              </div>
              <div>
                <h2>Barcelona</h2>
              </div>
              <div class="ciudad">
                <img src={NewYork} alt="New York" />
              </div>
              <div>
                <h3>New York</h3>
              </div>
            </div>
            <div class="columnader">
              <div class="ciudad">
                <img src={Maracaibo} alt="Maracaibo" />
              </div>
              <div>
                <h2>Maracay</h2>
              </div>
              <div>
                <img src={Valencia} alt="Valencia" />
              </div>
              <div>
                <h3>Valencia</h3>
              </div>
            </div>
          </div>
          <div>
            <div class="columnaizq">
              <div class="ciudad">
                <img src={Tokyo} alt="Tokyo" />
              </div>
              <div>
                <h2>Tokyo</h2>
              </div>
              <div class="ciudad">
                <img src={Grecia} alt="Grecia" />
              </div>
              <div>
                <h3>Grecia</h3>
              </div>
            </div>
            <div class="columnader">
              <div class="ciudad">
                <img src={Madrid} alt="Madrid" />
              </div>
              <div>
                <h2>Madrid</h2>
              </div>
              <div class="ciudad">
                <img src={Dubai} alt="Dubai" />
              </div>
              <div>
                <h3>Dubai</h3>
              </div>
            </div>
          </div>
          <div>
            <div class="columnaizq">
              <div class="ciudad">
                <img src={Paris} alt="Paris" />
              </div>
              <div>
                <h2>Paris</h2>
              </div>
              <div class="ciudad">
                <img src={Venecia} alt="Venecia" />
              </div>
              <div>
                <h3>Venecia</h3>
              </div>
            </div>
            <div class="columnader">
              <div class="ciudad">
                <img src={Estambul} alt="Estambul" />
              </div>
              <div>
                <h2>Estambul</h2>
              </div>
              <div class="ciudad">
                <img src={Londres} alt="Londres" />
              </div>
              <div>
                <h3>Londres</h3>
              </div>
            </div>
          </div>
        </Slider>
      </div>
    );
  }
}

export default Carrusel;
