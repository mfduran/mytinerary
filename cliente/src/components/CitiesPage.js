import React, { Component } from "react";
import { connect } from "react-redux";
import Footer from "./Footer";
import { getCitys } from "../actions/citiesActions";
import Header from "./Header";
import Banner from "./banner";

class Cities extends Component {
  constructor() {
    super();
    this.state = {
      filter: ""
    };
  }

  updateFilter(event) {
    const e = event.target.value;
    setTimeout(() => {
      this.setState({
        filter: e.substr(0, 20)
      });
    }, 300);
  }

  searchCities(citys) {
    return citys.filter(city => {
      return (
        city.city.toLowerCase().indexOf(this.state.filter.toLowerCase()) !== -1
      );
    });
  }
  renderCitys(filteredCitys) {
    return filteredCitys.length !== 0 ? (
      filteredCitys.map(city => <Banner city={city} />)
    ) : (
      <h3>No Hay Ciudades</h3>
    );
  }
  componentDidMount() {
    if (!this.props.citys.length) {
      this.props.getCitys();
    }
  }
  render() {
    let filteredCitys = this.searchCities(this.props.citys);
    return (
      <div className="container mt-2">
        <Header />
        <input
          type="text"
          placeholder="Filter by City"
          className="mb-2"
          value={this.state.search}
          onChange={this.updateFilter.bind(this)}
        />
        {this.props.loading ? (
          <h3>Loading</h3>
        ) : this.props.error ? (
          <h3>{this.props.error}</h3>
        ) : this.props.citys ? (
          this.renderCitys(filteredCitys)
        ) : (
          ""
        )}
        <br></br>
        <br></br>
        <Footer />
      </div>
    );
  }
}
const mapStateToProps = reducers => {
  return reducers.cityreducer;
};
export default connect(
  mapStateToProps,
  { getCitys }
)(Cities);
