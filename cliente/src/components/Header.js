import React, { Component } from "react";
import Logo from "../images/MYtineraryLogo.png";

class Header extends Component {
  render() {
    return (
      <div>
        <img
          className="col-sm-12 w-100 p-0 mb-5"
          src={Logo}
          alt="LogoMytinerary"
        />
      </div>
    );
  }
}
export default Header;
