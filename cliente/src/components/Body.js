import React, { Component } from "react";
import { Link } from "react-router-dom";
import Carrusel from "./Carrusel";
import Flecha from "../images/circled-right-2.png";
import Header from "./Header";
class Body extends Component {
  render() {
    return (
      <div>
        <Header />
        <div className="text-center">
          <p className="mb-5">
            <b>
              Find your perfect trip, designed by insiders who know and love
              their cities.
            </b>
          </p>
        </div>
        <div className="text-center">
          <Link to="/citiespage">
            <img
              className="col-sm-12 w-25 mb-4"
              src={Flecha}
              alt="flecha apuntando"
            />{" "}
          </Link>
        </div>
        <Carrusel />
      </div>
    );
  }
}
export default Body;
